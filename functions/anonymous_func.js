// simple anonymous function
// task 1)
let value=function (name,age){
    if(name=='rock' && age == 25){
        return ('my name is '+ name + ' and i am '+ age+ ' year old'  )
    }else{
        return ('error')
    }
}
console.log(value('rock',25))


// task 2)
let reflect=function (array){
    let sum=0
    for(let i=0;i<array.length;i++){
        sum += array[i]
    }
    return sum
}
console.log(reflect([1,2,3,4]))



// task 3)
// to wait loop execution
let time = function (Data){
    for(let i=0;i<Data.length;i++)
    setTimeout(() => {
        console.log(Data[i])
    }, 2000*i+1);
}
time([1,2,3,4,6,8])
