//types of functions
// A. Named functions

// task 1. create a function to calculate the percentage
// function percentage(a, b) {
//     let percent = (a / b) * 100;    // formula for percentage
//     console.log(`The total Marks obtained in percentage is ${percent}%`);
// }
// percentage(55, 70)

///////////////////////////////////////

// function add (num){
// let sum=0
// for(let i=0;i<=num;i++){
//     sum+=i
// }
// console.log(sum);
// }
// add(10)


// B. anonymous functions
//1. anonymous functions => functions without name store in variable

// let count = function (num) {
//     let sum = 0;
//     for (let i = 0; i <= num; i++) {
//         sum += i;                   // sum = sum+i
//     }
//     console.log(sum);
// }
// count(10)



// 2. function to print table 
// let table = function (num) {
//     for(let i=1; i<=10; i++){
//         result = i*num;          //logic for table
//         console.log(result);
//     }
// }
// table(2)


// 3. function to print table  within the range
// let table = function (start,end) {
//     for(let i=start; i<=end; i++){
//         for(j=1;j<=10;j++){
//             result = i*j;        //logic for table
//         console.log(result);
//         }
//     }
// }
// table(2,5)

// C . Immidietely invoke function expression=> it is a function that runs as soon as it define
// points to be remember in IIFE 
// 1. it contains anonymous function
// 2. it must have grouping operator
// 3. function must be called immidietetly after the defining of function

// 1. program to convert degree into fahrenheit
//(1°C × 9/5) + 32 = 33.8°F
// ( function (){
//     var degree = 5; // variable will not eccess by outside
//     var fahrenheit = degree *(9/5)+32 ;  // formula 
//     console.log(`${degree} degree Celsius will be equal to ${fahrenheit} fahrenheit`);}
// )()
//////////////////////////
// add number
// (function (num){
//     let sum=0
//     for(let i=0;i<=num;i++){
// sum+=i
//     }
//     console.log(sum);
// })(10)