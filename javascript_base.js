
//  const random = ['tree', 795, [0, 1, 2,[0, 2]]];
// console.log(random[2][3][1]);

//** adding the element in array

// const myArray = ['Manchester', 'Liverpool'];
// const name1 = "barcelona"
// console.log(myArray.push(name1)) // returns length of the array
// console.log(myArray.length);
// console.log(myArray);// push at the end of an array


// const found = ['Manchester', 'Liverpool'];
// found.unshift('Edinburgh');  // add at the start of the array
// console.log(found);     // [ "Edinburgh", "Manchester", "Liverpool" ]




//** removing  the element in array

// const game  = ['Manchester', 'Liverpool'];
// console.log(game.pop())  // remove last item from an array

// const cricket = ['sachin','watson', 'dhoni']
// cricket[0]= 'dravid'
// console.log(cricket);
// console.log(cricket.shift()) // remove item from  the start of the array
//  console.log(cricket);  // returning the remaining array



// const myArray = ['Manchester', 'Liverpool', 'Edinburgh', 'Carlisle'];
// let found_value=myArray.splice(1) // return element at index number in an array
// console.log(myArray);  
// console.log(found_value);


// slice in array
// let fruit= ["Strawberry", "Mango",'Parrot','banana','apple']
// let copy = fruit.slice(2) // return element present on index number
// console.log(copy);   // o/p ["Strawberry"]


// let thanos = 'i am inevitable'
// let value = thanos.slice(5,8)// count from 5 to befone 8 or say 7 form 0 index
// console.log(value);
// console.log(thanos.toLocaleLowerCase())
// console.log(thanos.toUpperCase())


// ** accessing every item 

// const birds = ['Parrot', 'Falcon', 'Owl'];

// for (const bird of birds) {
//   console.log(bird);
// }


// // using map 
// function add (number){
//     return number + 2
// }
// value= [1,2,3,4,5]
// const answer =value.map(add)
// console.log(answer); 

// // using filter
// const cities = ['London', 'Liverpool', 'Totnes', 'Edinburgh'];
// let area = cities.filter((ele)=>{
// if (ele.length>6){
//     return(ele)
// }

// })
// console.log(area);  


// ** converting between string and array

// split
// const myData = 'Manchester,London,Liverpool,Birmingham,Leeds,Carlisle';
// let str = myData.split(',')
// console.log(str.length-1);  
// console.log(str);  

// join
// let data=['Manchester','London','Liverpool','Birmingham','Leeds','Carlisle']
// let  result=data.join(' and ') 
// console.log(result);

// to string
// const dogNames = ['Rocket','Flash','Bella','Slugger'];
// let final = dogNames.toString()  // convert array into the string
// console.log(final) //return string 

// ** methods of array


// const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
// let result = words.filter((ele)=>{
//     if(ele=='limit'){
//         console.log('present');
//         return ele
//     }
    
// })
// console.log(result);  

// map  is a method of array which returns the o/p in array form
// const array1 = ['a', 'b', 'c'];
// let result = array1.map((ele)=>{
//     if(ele=='c'){
//         return array1[ele]="value"
//     }
//     else{
//         return ele
//     }
// })
// console.log(result); 

// * closures 

// function outerfunc (a){
//     let value = 10;
//     function innerfunc (){
//         console.log(`the out put using closure is ${a+value} `);
//     }
//     innerfunc()
// }
// outerfunc(20)

////////

// let parent = function (b){
//     let a = 'node'
//     let child = ()=>{
//         console.log(`${a} is a runtime  environment of ${b}`)
//     }
//     child()
// }
// parent('javascript')

// Hoisting in js => js already allocate undefine to variable and same function  b4 code run
// work only on var and named functions
// a=10     // initialization
// console.log(a)  // can access a  before Declration
// var a;  // Declration

getstudents()
function getstudents(){
console.log('getting details of students');
}





