// // // callback => use to write asynchronous code

// // // //error first callback using fs module

// const fs = require('fs')
// const file = ('file.txt')

// function present (error,data){
// if(error){
//     console.log(error)
// }
// else {
//        console.log('run successfully ');

//     console.log(data.toString());
// }
// }
// fs.readFile(file,present)


// // //**setTimeout */   

// // function time (){
// //     console.log('logging after 3 seconds');
// // }

// // setTimeout(time,3000)


// // // callback /

// // // using traditional function

// // //print table with interval of time
// // function table(number, statement) {
// //     console.log(`Displaying the table of ${number}`)
// //     statement(number)  // always pass parameter when passing parameter in second argument
// // }
// // let count = (number) => {
// //     let display = 0
// //     for (let i = 1; i <= 10; i++) {
// //         setTimeout(() => {  // always write settimeout within the for loop 
// // 			//bcoz if for loop copmlete first then timeout will not get value of i
// //             display = number * i
// //             console.log(display);
// //         }, 2000 * i)
// //     }
// // }
// // table(2, count)


// // task to count a number

// // function number (a){
// // console.log('enter the number for counting')
// // a(10)
// // }

// // function result (num){
// //     let count = 0;
// //     for(let i=0;i<num;i++){
// //        count = count + i

// //     }
// //     console.log(count);
// // }

// // number(result)

// // callback using string

// // function test(err,cb){
// // if(err){
// //     console.log(err)
// // }
// // cb('string')
// // }
// // function check(b){
// //     if(b=='string'){
// //         console.log('true')
// //     }
// //     else{
// //         console.log('false');
// //     }
// // }
// // test(check)

// function check(data, calling) {
//     let value = calling('found erro', 'string')

//     if (value == data) {
//         console.log('condition match');
//     } else {
//         console.log('data not found')
//     }
// }

// function found(error, a) {
//     if (error == "found error") {
//         return error
//     }
//     return a

// }
// check('string', found)


///////////////////////////////////////

// function test(string, cb) {
//     if (string == 'ab') {
//         return cb(null, 'success')
//     } else {
//         return cb('error', null)
//     }
// }
// test('ab', test2)

// function test2(error, data) {
//     if (error) {
//         console.log(error)
//         return error

//     } else {
//         console.log(data);
//         return data
//     }
// }

// function test(string,cb){
//     if(string=='nimap'){
//         return cb(null,'success')
//     }else{
//         return cb('error found',null)
//     }
// }

// function check(error,data){
//     if(error){
//         return error
//     } else{
//         return data
//     }
// }
// console.log(test('imap',check))

function test(string,cb){
if(string=='present'){
    return cb(null,'success')
}else{
    return cb(error,null)
 }
}
function check(error,data){
    if(error){
        return error
    } else{
        return data
    }
}
console.log(test('present',check))
