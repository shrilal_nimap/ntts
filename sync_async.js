// // ** synchronous (code run line by line)
// let func =()=> {
//     console.log('code run synchronously');  // execute 3
// }
// console.log('usain bolt is a olympic winner')  // execute 1

// function run() {
//     console.log("olympic is a best platform for athletes")  // execute 2
//     func()
// }
// run()
// console.log('olympic over')     // execute 4

// example 2 
// function syncr() {
//     console.log('function executed')
// }
// console.log('execute first')
// syncr()
// console.log('working in synchronous way');



// ** Asynchronous (code does't wait for other code or not execute line by line )
// it is also called as make js async
// 1)task

// function book() {
//   console.log('reading book is a good habit'); // execute 3
// }

// let success =()=>{
//     console.log('to become a successful consume one habit like') // execute 1
//     setTimeout(()=>{
//         book()

//     },3000)
// }
// success()

//  console.log('Asynchronous function works');  // execute 2  not waiting for book()...it is behaving async

//2) task

// function price(){
//     console.log('laptop');
// }

// setTimeout(()=>{
//     console.log('price is very low');

// },3000)
// price()

// console.log('working asynchronously'); // not waiting for timeout function


//////////////////

function run(){
    setTimeout(()=>{
        console.log(1)
    },2000)
    console.log('running first ')
}
run()
console.log('outside settimeout')